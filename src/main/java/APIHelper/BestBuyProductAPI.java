package APIHelper;

import java.util.HashMap;

import com.sandeep.RESTFW.Setup.TestSetup;
import com.sandeep.RESTFW.Utils.RestTemplate;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class BestBuyProductAPI extends TestSetup
{

    /*
     * Make sure you have NodeJS installed (we require version 4 or newer). git clone
     * https://github.com/bestbuy/api-playground/ cd api-playground npm install npm start # Best Buy API Playground
     * started at http://localhost:3030 Now open http://localhost:3030 in your browser to begin exploring the API. From
     * there we'll guide you on using tools such as Swagger and Postman to get meaningful experience interacting with
     * APIs.
     */

    public static RequestSpecification requestspecification;

    public static Response response;

    public static Response BestBuyCreateProduct(String name, String type, float price, String upc, String description,
        String model)
    {

        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();
        HashMap<String, Object> body = new HashMap<String, Object>();
        // body.put("name", "Lungi");
        // body.put("type", "cloth");
        // body.put("price", 10.10);
        // body.put("shipping", 0.0);
        // body.put("upc", "12345668");
        // body.put("description", "This is a placeholder request for creating a new product.");
        // body.put("model", "NM12345");

        body.put("name", name);
        body.put("type", type);
        body.put("price", price);
        // body.put("shipping", configProperties.getshipping());
        body.put("upc", upc);
        body.put("description", description);
        body.put("model", model);

        requestspecification = requestbuilder.addHeader("Content-Type", "application/json").setBody(body).build();
        TestSetup.logDetails(body, "Requestbody");

        Response response = RestTemplate.post(requestspecification);

        TestSetup.logDetails(response.asString(), "Respone");

        return response;

    }

    public static Response BestBuyCreateProduct(String name, String type, double price, String upc, String description,
        String model)
    {
        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();
        HashMap<String, Object> body = new HashMap<String, Object>();
        // body.put("name", "Lungi");
        // body.put("type", "cloth");
        // body.put("price", 10.10);
        // body.put("shipping", 0.0);
        // body.put("upc", "12345668");
        // body.put("description", "This is a placeholder request for creating a new product.");
        // body.put("model", "NM12345");

        body.put("name", name);
        body.put("type", type);
        body.put("price", price);
        // body.put("shipping", configProperties.getshipping());
        body.put("upc", upc);
        body.put("description", description);
        body.put("model", model);

        requestspecification = requestbuilder.addHeader("Content-Type", "application/json").setBody(body).build();
        TestSetup.logDetails(body, "Requestbody");

        Response response = RestTemplate.post(requestspecification);

        TestSetup.logDetails(response.asString(), "Respone");

        return response;
    }

    public static Response BestBuyCreateProduct()
    {
        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();

        HashMap<String, Object> body = new HashMap<String, Object>();

        body.put("name", null);
        body.put("type", null);
        body.put("price", null);
        // body.put("shipping", configProperties.getshipping());
        body.put("upc", null);
        body.put("description", null);
        body.put("model", null);

        requestspecification = requestbuilder.addHeader("Content-Type", "application/json").setBody(body).build();

        TestSetup.logDetails(body, "Requestbody");

        Response response = RestTemplate.post(requestspecification);

        TestSetup.logDetails(response.asString(), "Respone");

        return response;

    }

}
