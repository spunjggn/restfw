package APIHelper;

import org.json.simple.JSONObject;

import com.aventstack.extentreports.Status;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sandeep.RESTFW.RequestPOJOClasses.Fields;
import com.sandeep.RESTFW.RequestPOJOClasses.Issue;
import com.sandeep.RESTFW.RequestPOJOClasses.Issuetype;
import com.sandeep.RESTFW.RequestPOJOClasses.Parent;
import com.sandeep.RESTFW.RequestPOJOClasses.Project;
import com.sandeep.RESTFW.Setup.TestSetup;
import com.sandeep.RESTFW.Utils.FinalConstants;
import com.sandeep.RESTFW.Utils.RestTemplate;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class JiraHelperAPI extends TestSetup
{

    public static RequestSpecification requestspecification;

    public static Response response;

    static String getCookie;

    // @Test(priority = 1)
    public static Response generateCookie()
    {

        // RestAssured.baseURI = configProperties.getJiraURI();
        // RestAssured.basePath = configProperties.getJiraBasePath();

        // System.out.println("Url with end point is " + requestspecification);
        // System.out.println("Url is " + RestAssured.baseURI + " basePath is " + RestAssured.basePath);

        Logger().log(Status.INFO, "The username and Password is " + FinalConstants.JIRA_USER_NAME);
        JSONObject json = new JSONObject();
        json.put("username", FinalConstants.JIRA_USER_NAME);
        json.put("password", FinalConstants.JIRA_PASSWORD);

        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();
        requestspecification = requestbuilder.addHeader("Content-Type", "application/json").setBody(json).build();
        return (RestTemplate.post(requestspecification, configProperties.getJiraCookieEndPoint()));

    }

    // @Test(priority = 2, dependsOnMethods = "generateCookie")
    //
    // public static void createtask()
    // {
    // String bdy =
    // "{ \"fields\": { \"project\": { \"key\": \"SPA\" }, \"summary\": \"My first task via APi.\", \"description\":
    // \"First API call using post method using cookies for authentication\", \"issuetype\": { \"name\": \"Task\" } }}";
    // Response response = RestAssured.given().header("Content-Type", "application/json")
    // .cookie("JSESSIONID", getCookie).body(bdy).post("http://localhost:8070/rest/api/2/issue").andReturn();
    // System.out.println(response.getStatusCode());
    // System.out.println(response.asString());
    //
    // }

    // public static void taskCreation()
    // {
    // String getCookie = JiraHelperAPI.generateCookie().cookies().get("JSESSIONID");
    //
    // ObjectMapper objmapper = new ObjectMapper();
    // Issuetype issuetype = new Issuetype("Task");
    // Project project = new Project("SPA");
    // Parent parent = new Parent();
    //
    // Fields fields = new Fields("Creating a Subtask", issuetype, project, "Create Restassured Test Suite", parent);
    //
    // Issue issue = new Issue(fields);
    // // issue.getFields().getProject().setKey("SPA");
    //
    // System.out.println("" + issue.toString());
    //
    // Response response = RestAssured.given().header("Content-Type", "application/json")
    // .cookie("JSESSIONID", getCookie).body(issue).post("http://localhost:8070/rest/api/2/issue").andReturn();
    // System.out.println(response.getStatusCode());
    // System.out.println(response.asString());
    //
    //
    // RequestSpecBuilder requestbuilder = new RequestSpecBuilder();
    // requestspecification = requestbuilder.addHeader("Content-Type", "application/json")
    // .addCookie("JSESSIONID", getCookie).setBody(issue).build();
    // return (RestTemplate.post(requestspecification, endPoint))
    //
    // }

    public static Response createIssueAPI(String summary, String description, String issuetypes, String projectName)
    {
        Logger().log(Status.INFO, "Getting the cookies for the JSESSION");
        String getCookie = JiraHelperAPI.generateCookie().cookies().get("JSESSIONID");
        Logger().log(Status.INFO, "Cookie for the JSESSION is " + getCookie);
        ObjectMapper objmapper = new ObjectMapper();
        Issuetype issuetype = new Issuetype(issuetypes);
        Project project = new Project(projectName);
        Parent parent = new Parent();

        Fields fields = new Fields(summary, issuetype, project, description, parent);

        Issue issue = new Issue(fields);

        TestSetup.logDetails(String.valueOf(issue), "Request");

        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();

        requestspecification = requestbuilder.addHeader("Content-Type", "application/json")
            .addCookie("JSESSIONID", getCookie).setBody(issue).build();

        return (RestTemplate.post(requestspecification, configProperties.getJiraIssueEndPoint()));

    }

    public static Response createIssueAPI(String summary, String description, String issuetypes, String projectName,
        String parentID)
    {
        Logger().log(Status.INFO, "Getting the cookies for the JSESSION");
        String getCookie = JiraHelperAPI.generateCookie().cookies().get("JSESSIONID");
        Logger().log(Status.INFO, "Cookie for the JSESSION is " + getCookie);

        ObjectMapper objmapper = new ObjectMapper();
        Issuetype issuetype = new Issuetype(issuetypes);
        Project project = new Project(projectName);
        Parent parent = new Parent(parentID);

        Fields fields = new Fields(summary, issuetype, project, description, parent);

        Issue issue = new Issue(fields);

        TestSetup.logDetails(String.valueOf(issue), "Request");

        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();

        requestspecification = requestbuilder.addHeader("Content-Type", "application/json")
            .addCookie("JSESSIONID", getCookie).setBody(issue).build();

        return (RestTemplate.post(requestspecification, configProperties.getJiraIssueEndPoint()));

    }

    public static Response assigntaskAPI()
    {
        Logger().log(Status.INFO, "Getting the cookies for the JSESSION");
        String getCookie = JiraHelperAPI.generateCookie().cookies().get("JSESSIONID");
        Logger().log(Status.INFO, "Cookie for the JSESSION is " + getCookie);

        JSONObject json = new JSONObject();
        json.put("name", FinalConstants.JIRA_ASSIGNEE);

        JSONObject pobj = new JSONObject();
        pobj.put("assignee", json);

        JSONObject body = new JSONObject();

        body.put("fields", pobj);

        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();

        TestSetup.logDetails(String.valueOf(body), "Request");

        requestspecification = requestbuilder.addHeader("Content-Type", "application/json")
            .addCookie("JSESSIONID", getCookie).setBody(body).build();

        return (RestTemplate.put(requestspecification, configProperties.getJiraIssueEndPointForAssignment()));

    }

    public static Response addingAComnentAPI()
    {
        Logger().log(Status.INFO, "Getting the cookies for the JSESSION");
        String getCookie = JiraHelperAPI.generateCookie().cookies().get("JSESSIONID");
        Logger().log(Status.INFO, "Cookie for the JSESSION is " + getCookie);

        JSONObject json = new JSONObject();

        json.put("body", FinalConstants.JIRA_UPDATED_COMMENT);

        TestSetup.logDetails(String.valueOf(json), "Request");

        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();

        requestspecification = requestbuilder.addHeader("Content-Type", "application/json")
            .addCookie("JSESSIONID", getCookie).setBody(json).build();

        return (RestTemplate.post(requestspecification, configProperties.getJiraIssueEndPointForComment()));

    }
}
