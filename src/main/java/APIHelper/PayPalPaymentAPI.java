package APIHelper;

import com.sandeep.RESTFW.Setup.TestSetup;
import com.sandeep.RESTFW.Utils.FinalConstants;
import com.sandeep.RESTFW.Utils.RestTemplate;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PayPalPaymentAPI extends TestSetup
{
    static String accessToken;

    public static RequestSpecification requestspecification;

    public static Response response;

    public static Response PayPalPaymentAuth()
    {
        accessToken = generateTokenUtil();
        System.out.println("AccessToken  " + accessToken);

        RestAssured.baseURI = configProperties.getPayPalBaseURI();
        RestAssured.basePath = configProperties.getPayPalPayment();
        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();
        requestspecification = requestbuilder.addHeader("Authorization", "Bearer " + accessToken)
            .addHeader("Content-Type:", "application/json").build();
        System.out.println("RSpecificaton " + requestspecification);
        return (RestTemplate.get(requestspecification));

    }

    public static String generateTokenUtil()
    {
        /* The base Jira URI is changed from URL, please if errors are coming while running */
        // System.out.println("" + configProperties.getJiraURI());
        RestAssured.baseURI = configProperties.getPayPalBaseURI();
        RestAssured.basePath = configProperties.getPayPalAuthPath();
        accessToken = RestAssured.given().auth().preemptive()
            .basic(configProperties.getPayPalUser(), configProperties.getPaylPalPassword())
            .formParam("grant_type", FinalConstants.PAYPAL_GRANT_TYPE).log().all()
            .header("Content-Type", "application/x-www-form-urlencoded").when().post().body().jsonPath()
            .get("access_token");
        System.out.println("" + accessToken);
        return accessToken;
    }

}
