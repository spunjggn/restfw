package APIHelper;

import com.sandeep.RESTFW.RequestPOJOClasses.Address;
import com.sandeep.RESTFW.RequestPOJOClasses.Billing_details;
import com.sandeep.RESTFW.Setup.TestSetup;
import com.sandeep.RESTFW.Utils.RestTemplate;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class StripeCharge extends TestSetup
{

    public static RequestSpecification requestspecification;

    public static Response createCharge(String EndPoint, String currency, String Desc, String source, String Key,
        int Amount)
    {

        Address address = new Address();
        address.setLine1("Gurgaon");
        address.setCity("Gurgaon");
        address.setState("Haryana");
        address.setPostal_code("122001");
        address.setCountry("India");

        Billing_details billing_details = new Billing_details();
        billing_details.setAddress(address);

        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();
        requestspecification = requestbuilder.addFormParam("description", Desc).addFormParam("amount", Amount)
            .addFormParam("currency", currency).addFormParam("source", source)
            .addHeader("Authorization", "Bearer " + Key).build();

        // .addFormParam("billing_deatils", billing_details).addFormParam("name", "Sandeep")
        // .addFormParam("address", address).addFormParam("name", "Sandeep").addFormParam("country", "India")
        Response response = RestTemplate.post(requestspecification, EndPoint);
        return response;

    }

}
