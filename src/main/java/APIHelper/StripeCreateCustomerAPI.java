package APIHelper;

import org.json.simple.JSONObject;

import com.sandeep.RESTFW.RequestPOJOClasses.Customer;
import com.sandeep.RESTFW.Setup.TestSetup;
import com.sandeep.RESTFW.Utils.RestTemplate;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class StripeCreateCustomerAPI extends TestSetup
{

    public static RequestSpecification requestspecification;

    public static Response response;

    public static Response postCreateCustomerAPI(String Key, String desc)
    {

        RestAssured.baseURI = "https://api.stripe.com/";
        RestAssured.basePath = "v1/customers";
        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();
        requestspecification =
            requestbuilder.addFormParam("description", desc).addHeader("Authorization", "Bearer " + Key).build();
        Response response = RestTemplate.post(requestspecification);
        return response;
    }

    public static Response getCustomerAPI(String Key, String custid)
    {

        RestAssured.baseURI = "https://api.stripe.com/";
        RestAssured.basePath = "v1/customers/";
        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();
        requestspecification = requestbuilder.addHeader("Authorization", "Bearer " + Key).build();
        Response response = RestTemplate.get(requestspecification, custid);
        return response;
    }

    public static Response deleteCustomerAPI(String Key, String custid)
    {
        RestAssured.baseURI = "https://api.stripe.com/";
        RestAssured.basePath = "v1/customers/";
        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();
        requestspecification = requestbuilder.addHeader("Authorization", "Bearer " + Key).build();
        Response response = RestTemplate.delete(requestspecification, custid);

        return response;

    }

    public static Response postUpdateCustomerAPI(String Key, String endPoint)
    {

        RestAssured.baseURI = "https://api.stripe.com/";
        RestAssured.basePath = "v1/customers";

        // Metadata met = new Metadata("1234");
        // // metadata.setOrder_id("1234");
        Customer customer = new Customer();

        JSONObject json = new JSONObject();
        json.put("order_id", "6735");

        RequestSpecBuilder requestbuilder = new RequestSpecBuilder();
        requestspecification = requestbuilder.addFormParam("description", "Sandeep_new1")
            .addHeader("Authorization", "Bearer " + Key).build();

        // requestspecification =
        // requestbuilder.addFormParam("metadata", json).addHeader("Authorization", "Bearer " + Key).build();

        // requestspecification =
        // requestbuilder.addFormParam("metadata", "metadata").addHeader("Authorization", "Bearer " + Key).build();

        // requestspecification = requestbuilder.addHeader("Authorization", "Bearer " + Key)
        // .addHeader("Content-Type", "application/json").setBody(customer).build();

        Response response = RestTemplate.post(requestspecification, endPoint);
        return response;
    }

}
