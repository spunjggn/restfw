package com.sandeep.RESTFW.DBHelper;

import java.sql.SQLException;
import java.util.LinkedHashMap;

public interface DataBaseHelper
{
    public DataBaseHelper setConnectionString(String connectionString);

    public int executeUpdate(String sqlQuery) throws ClassNotFoundException, SQLException;

    public LinkedHashMap<Integer, LinkedHashMap<Object, Object>> executeQuery(String sqlQuery)
        throws ClassNotFoundException, SQLException;

    // public LinkedHashMap<Integer, LinkedHashMap<String, String>> executeQuery(String sqlQuery)
    // throws ClassNotFoundException, SQLException;

}
