package com.sandeep.RESTFW.DBHelper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;

public interface DataBaseHelper1
{

    public Connection getconnection(String connectionString, String user, String password)
        throws ClassNotFoundException, SQLException;

    public int executeUpdate(String sqlQuery) throws SQLException, ClassNotFoundException;

    /*
     * <Integer will store row number, Inner Hashmap will have columnName and Value in key value Pair>
     */
    public LinkedHashMap<Integer, LinkedHashMap<String, String>> executeQuery(String sqlQuery)
        throws SQLException, ClassNotFoundException;

    DataBaseHelper1 setConnectionString(String connectionString);

}
