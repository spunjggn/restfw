package com.sandeep.RESTFW.DBHelper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.LinkedHashMap;

public class MySqlHelper implements DataBaseHelper
{

    private String connectionString;

    private Connection connection = null;

    private Statement statement = null;

    public DataBaseHelper setConnectionString(String connectionString)
    {
        // connectionString = "\"jdbc:mysql://localhost:3306/employeeportal\", \"root\", \"root\"";

        System.out.println(connectionString);

        this.connectionString = connectionString;
        return this;
    }

    private Connection getConnection() throws SQLException, ClassNotFoundException
    {

        Class.forName("com.mysql.jdbc.Driver");
        System.out.println("Data base connection is successful");
        // DriverManager.registerDriver(new com.mysql.jdbc.Driver());

        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/employeeportal", "root", "root");
        return connection;
        // return DriverManager.getConnection(connectionString);

    }

    private Statement getStatement() throws ClassNotFoundException, SQLException
    {
        return getConnection().createStatement();
    }

    public int executeUpdate(String sqlQuery) throws ClassNotFoundException, SQLException
    {
        return getStatement().executeUpdate(sqlQuery);

    }

    public LinkedHashMap<Integer, LinkedHashMap<Object, Object>> executeQuery(String sqlQuery)
        throws ClassNotFoundException, SQLException
    {
        ResultSet resultSet = getStatement().executeQuery(sqlQuery);
        return getData(resultSet);
    }

    // public LinkedHashMap<Integer, LinkedHashMap<String, String>> executeQuery(String sqlQuery)
    // throws ClassNotFoundException, SQLException
    // {
    // ResultSet resultSet = getStatement().executeQuery(sqlQuery);
    // return getData(resultSet);
    // }

    private LinkedHashMap<Integer, LinkedHashMap<Object, Object>> getData(ResultSet resultSet) throws SQLException
    {
        if (resultSet == null)
            throw new RuntimeException("Result Set is Null");

        ResultSetMetaData columndata = resultSet.getMetaData();

        String[] columnName = getColumnName(columndata);
        int counter = 1;
        LinkedHashMap<Integer, LinkedHashMap<Object, Object>> dbData =
            new LinkedHashMap<Integer, LinkedHashMap<Object, Object>>();
        while (resultSet.next()) {
            dbData.put(counter, getData(columnName, resultSet));
            counter++;
        }
        return dbData;
    }

    private LinkedHashMap<Object, Object> getData(String[] columnName, ResultSet resultSet) throws SQLException
    {
        LinkedHashMap<Object, Object> data = new LinkedHashMap<Object, Object>();
        for (int i = 0; i < columnName.length; i++) {
            data.put(columnName[i], getDataBasedOncolumnType(i, resultSet));
        }
        return data;
    }

    private String getDataBasedOncolumnType(int i, ResultSet resultSet) throws SQLException
    {
        int type = resultSet.getMetaData().getColumnType(i + 1);
        switch (type) {
            case Types.VARCHAR:
                return resultSet.getString(i + 1);
            case Types.NUMERIC:
                return resultSet.getInt(i + 1) + "";
            case Types.INTEGER:
                return resultSet.getInt(i + 1) + "";
            case Types.OTHER:
                return resultSet.getInt(i + 1) + "";
            case Types.DISTINCT:
                return resultSet.getInt(i + 1) + "";

        }
        return null;
    }

    private String[] getColumnName(ResultSetMetaData columndata) throws SQLException
    {
        String[] columnName = new String[columndata.getColumnCount()];
        for (int i = 1; i <= columndata.getColumnCount(); i++) {
            columnName[i - 1] = columndata.getColumnName(i);
        }
        return columnName;
    }
}
