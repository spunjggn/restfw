package com.sandeep.RESTFW.DBHelper;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.LinkedHashMap;

public class MySqlHelper1 implements DataBaseHelper1, Closeable
{

    private Connection connection = null;

    private Statement statement = null;

    private ResultSet result = null;

    public Connection getdbconnection(String connectionString, String user, String password)
        throws ClassNotFoundException, SQLException
    {
        Class.forName("com.mysql.jdbc.Driver");
        // System.out.println("" + DriverManager.getConnection(connectionString, user, password).toString());
        // return DriverManager.getConnection(connectionString, user, password);
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/employeeportal", "root", "root");
        return connection;

    }

    private Statement getStatement() throws SQLException
    {
        return connection.createStatement();
    }

    public int executeUpdate(String sqlQuery) throws SQLException
    {
        return statement.executeUpdate(sqlQuery);
    }

    public LinkedHashMap<Integer, LinkedHashMap<String, String>> executeQuery(String sqlQuery) throws SQLException
    {
        result = statement.executeQuery(sqlQuery);
        String[] columnName = getColumnName(result);

        LinkedHashMap<Integer, LinkedHashMap<String, String>> dbData = new LinkedHashMap();
        int counter = 1;
        while (result.next()) {
            dbData.put(counter, getdbdata(columnName, result));
            counter++;

        }

        return dbData;
    }

    private LinkedHashMap<String, String> getdbdata(String[] columnName, ResultSet result) throws SQLException
    {
        LinkedHashMap<String, String> columndata = new LinkedHashMap<String, String>();

        for (int i = 0; i <= columnName.length; i++) {
            /*
             * result.getString return String but we are not sure if return type is String integer etc so we need to add
             * another method here to find type of data(Columntype) and then use appropritate method to retrive
             * columndata In order to undrstand which columnName type has to be found we also pass index
             */

            // columndata.put(columnName[i], result.getString(i));

            columndata.put(columnName[i], getColumnData(i, result));
        }

        return columndata;
        // TODO Auto-generated method stub

    }

    private String getColumnData(int i, ResultSet result) throws SQLException
    {
        int type = result.getMetaData().getColumnType(i + 1);

        switch (type) {
            case Types.VARCHAR:

                return result.getString(i + 1);

            case Types.NUMERIC:
                return result.getInt(i + 1) + "";
        }

        return null;
    }

    private String[] getColumnName(ResultSet result) throws SQLException
    {
        ResultSetMetaData data = result.getMetaData();
        String[] columnName = new String[data.getColumnCount()];

        for (int i = 1; i <= data.getColumnCount(); i++) {
            columnName[i - 1] = data.getColumnName(i);
        }
        return columnName;
    }

    public void setConnectionString()
    {
        // Have some confusion in this, i need to understand why i need this

    }

    public void close() throws IOException
    {
        // logic of managing connection
        System.out.println("closing the resources");

    }

    public Connection getconnection(String connectionString, String user, String password)
        throws ClassNotFoundException, SQLException
    {
        // TODO Auto-generated method stub
        return null;
    }

    public DataBaseHelper1 setConnectionString(String connectionString)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
