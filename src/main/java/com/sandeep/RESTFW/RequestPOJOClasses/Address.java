package com.sandeep.RESTFW.RequestPOJOClasses;

public class Address
{
    private String country;

    private String city;

    private String state;

    private String postal_code;

    private String line2;

    private String line1;

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getPostal_code()
    {
        return postal_code;
    }

    public void setPostal_code(String postal_code)
    {
        this.postal_code = postal_code;
    }

    public String getLine2()
    {
        return line2;
    }

    public void setLine2(String line2)
    {
        this.line2 = line2;
    }

    public String getLine1()
    {
        return line1;
    }

    public void setLine1(String line1)
    {
        this.line1 = line1;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [country = " + country + ", city = " + city + ", state = " + state + ", postal_code = "
            + postal_code + ", line2 = " + line2 + ", line1 = " + line1 + "]";
    }
}
