package com.sandeep.RESTFW.RequestPOJOClasses;

public class Billing_details
{
    private Address address;

    private String phone;

    private String name;

    private String email;

    public Address getAddress()
    {
        return address;
    }

    public void setAddress(Address address)
    {
        this.address = address;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String

        getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String

        getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [address = " + address + ", phone = " + phone + ", name = " + name + ", email = " + email
            + "]";
    }
}
