package com.sandeep.RESTFW.RequestPOJOClasses;

public class Card
{
    private String country;

    private String last4;

    private String funding;

    private Checks checks;

    private String wallet;

    private String installments;

    private String fingerprint;

    private String exp_month;

    private String exp_year;

    private String three_d_secure;

    private String brand;

    private String network;

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getLast4()
    {
        return last4;
    }

    public void setLast4(String last4)
    {
        this.last4 = last4;
    }

    public String getFunding()
    {
        return funding;
    }

    public void setFunding(String funding)
    {
        this.funding = funding;
    }

    public Checks getChecks()
    {
        return checks;
    }

    public void setChecks(Checks checks)
    {
        this.checks = checks;
    }

    public String getWallet()
    {
        return wallet;
    }

    public void setWallet(String wallet)
    {
        this.wallet = wallet;
    }

    public String getInstallments()
    {
        return installments;
    }

    public void setInstallments(String installments)
    {
        this.installments = installments;
    }

    public String getFingerprint()
    {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint)
    {
        this.fingerprint = fingerprint;
    }

    public String getExp_month()
    {
        return exp_month;
    }

    public void setExp_month(String exp_month)
    {
        this.exp_month = exp_month;
    }

    public String getExp_year()
    {
        return exp_year;
    }

    public void setExp_year(String exp_year)
    {
        this.exp_year = exp_year;
    }

    public String getThree_d_secure()
    {
        return three_d_secure;
    }

    public void setThree_d_secure(String three_d_secure)
    {
        this.three_d_secure = three_d_secure;
    }

    public String getBrand()
    {
        return brand;
    }

    public void setBrand(String brand)
    {
        this.brand = brand;
    }

    public String getNetwork()
    {
        return network;
    }

    public void setNetwork(String network)
    {
        this.network = network;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [country = " + country + ", last4 = " + last4 + ", funding = " + funding + ", checks = "
            + checks + ", wallet = " + wallet + ", installments = " + installments + ", fingerprint = " + fingerprint
            + ", exp_month = " + exp_month + ", exp_year = " + exp_year + ", three_d_secure = " + three_d_secure
            + ", brand = " + brand + ", network = " + network + "]";
    }
}
