package com.sandeep.RESTFW.RequestPOJOClasses;

public class Customer
{
    public Customer()
    {

    }

    public Customer(String description)
    {
        this.description = description;
    }

    public Customer(Metadata metadata)
    {
        this.metadata = metadata;
    }

    private Invoice_settings invoice_settings;

    private Metadata metadata;

    private Subscriptions subscriptions;

    private String address;

    private String livemode;

    private Sources sources;

    private Tax_ids tax_ids;

    private String default_source;

    private String invoice_prefix;

    private String tax_exempt;

    private String created;

    private String next_invoice_sequence;

    private String description;

    private String discount;

    private String[] preferred_locales;

    private String balance;

    private String shipping;

    private String phone;

    private String delinquent;

    private String name;

    private String currency;

    private String id;

    private String email;

    private String object;

    public Invoice_settings getInvoice_settings()
    {
        return invoice_settings;
    }

    public void setInvoice_settings(Invoice_settings invoice_settings)
    {
        this.invoice_settings = invoice_settings;
    }

    public Metadata getMetadata()
    {
        return metadata;
    }

    public void setMetadata(Metadata metadata)
    {
        this.metadata = metadata;
    }

    public Subscriptions getSubscriptions()
    {
        return subscriptions;
    }

    public void setSubscriptions(Subscriptions subscriptions)
    {
        this.subscriptions = subscriptions;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getLivemode()
    {
        return livemode;
    }

    public void setLivemode(String livemode)
    {
        this.livemode = livemode;
    }

    public Sources getSources()
    {
        return sources;
    }

    public void setSources(Sources sources)
    {
        this.sources = sources;
    }

    public Tax_ids getTax_ids()
    {
        return tax_ids;
    }

    public void setTax_ids(Tax_ids tax_ids)
    {
        this.tax_ids = tax_ids;
    }

    public String getDefault_source()
    {
        return default_source;
    }

    public void setDefault_source(String default_source)
    {
        this.default_source = default_source;
    }

    public String getInvoice_prefix()
    {
        return invoice_prefix;
    }

    public void setInvoice_prefix(String invoice_prefix)
    {
        this.invoice_prefix = invoice_prefix;
    }

    public String getTax_exempt()
    {
        return tax_exempt;
    }

    public void setTax_exempt(String tax_exempt)
    {
        this.tax_exempt = tax_exempt;
    }

    public String getCreated()
    {
        return created;
    }

    public void setCreated(String created)
    {
        this.created = created;
    }

    public String getNext_invoice_sequence()
    {
        return next_invoice_sequence;
    }

    public void setNext_invoice_sequence(String next_invoice_sequence)
    {
        this.next_invoice_sequence = next_invoice_sequence;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String

        getDiscount()
    {
        return discount;
    }

    public void setDiscount(String discount)
    {
        this.discount = discount;
    }

    public String[] getPreferred_locales()
    {
        return preferred_locales;
    }

    public void setPreferred_locales(String[] preferred_locales)
    {
        this.preferred_locales = preferred_locales;
    }

    public String getBalance()
    {
        return balance;
    }

    public void setBalance(String balance)
    {
        this.balance = balance;
    }

    public String getShipping()
    {
        return shipping;
    }

    public void setShipping(String shipping)
    {
        this.shipping = shipping;
    }

    public String

        getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getDelinquent()
    {
        return delinquent;
    }

    public void setDelinquent(String delinquent)
    {
        this.delinquent = delinquent;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getObject()
    {
        return object;
    }

    public void setObject(String object)
    {
        this.object = object;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [invoice_settings = " + invoice_settings + ", metadata = " + metadata + ", subscriptions = "
            + subscriptions + ", address = " + address + ", livemode = " + livemode + ", sources = " + sources
            + ", tax_ids = " + tax_ids + ", default_source = " + default_source + ", invoice_prefix = " + invoice_prefix
            + ", tax_exempt = " + tax_exempt + ", created = " + created + ", next_invoice_sequence = "
            + next_invoice_sequence + ", description = " + description + ", discount = " + discount
            + ", preferred_locales = " + preferred_locales + ", balance = " + balance + ", shipping = " + shipping
            + ", phone = " + phone + ", delinquent = " + delinquent + ", name = " + name + ", currency = " + currency
            + ", id = " + id + ", email = " + email + ", object = " + object + "]";
    }
}
