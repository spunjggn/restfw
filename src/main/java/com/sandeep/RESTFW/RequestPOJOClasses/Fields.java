package com.sandeep.RESTFW.RequestPOJOClasses;

public class Fields
{
    public Fields(String summary, Issuetype issuetype, Project project, String description, Parent parent)
    {
        super();
        this.summary = summary;
        this.issuetype = issuetype;
        this.project = project;
        this.description = description;
        this.parent = parent;
    }

    public Fields()
    {
        // TODO Auto-generated constructor stub
    }

    private String summary;

    private Issuetype issuetype;

    private Project project;

    private String description;

    private Parent parent;

    public Parent getParent()
    {
        return parent;
    }

    public void setParent(Parent parent)
    {
        this.parent = parent;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public Issuetype getIssuetype()
    {
        return issuetype;
    }

    public void setIssuetype(Issuetype issuetype)
    {
        this.issuetype = issuetype;
    }

    public Project getProject()
    {
        return project;

    }

    public void setProject(Project project)
    {
        this.project = project;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public String toString()
    {
        return "Fields [summary=" + summary + ", issuetype=" + issuetype + ", project=" + project + ", description="
            + description + ", parent=" + parent + "]";
    }

}
