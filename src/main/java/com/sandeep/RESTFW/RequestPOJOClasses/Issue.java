package com.sandeep.RESTFW.RequestPOJOClasses;

public class Issue
{
    public Issue(Fields fields)
    {
        super();
        this.fields = fields;
    }

    public Issue()
    {

    }

    private Fields fields;

    public Fields getFields()
    {
        return fields;
    }

    public void setFields(Fields fields)
    {
        this.fields = fields;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [fields = " + fields + "]";
    }
}
