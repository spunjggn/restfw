package com.sandeep.RESTFW.RequestPOJOClasses;

public class Item_list
{
    private Shipping_address shipping_address;

    private Items[] items;

    public Shipping_address getShipping_address()
    {
        return shipping_address;
    }

    public void setShipping_address(Shipping_address shipping_address)
    {
        this.shipping_address = shipping_address;
    }

    public Items[] getItems()
    {
        return items;
    }

    public void setItems(Items[] items)
    {
        this.items = items;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [shipping_address = " + shipping_address + ", items = " + items + "]";
    }
}
