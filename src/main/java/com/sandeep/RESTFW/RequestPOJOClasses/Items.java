package com.sandeep.RESTFW.RequestPOJOClasses;

public class Items
{
    private String quantity;

    private String price;

    private String name;

    private String description;

    private String tax;

    private String currency;

    private String sku;

    public String getQuantity()
    {
        return quantity;
    }

    public void setQuantity(String quantity)
    {
        this.quantity = quantity;
    }

    public String getPrice()
    {
        return price;
    }

    public void setPrice(String price)
    {
        this.price = price;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getTax()
    {
        return tax;
    }

    public void setTax(String tax)
    {
        this.tax = tax;
    }

    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }

    public String getSku()
    {
        return sku;
    }

    public void setSku(String sku)
    {
        this.sku = sku;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [quantity = " + quantity + ", price = " + price + ", name = " + name + ", description = "
            + description + ", tax = " + tax + ", currency = " + currency + ", sku = " + sku + "]";
    }
}
