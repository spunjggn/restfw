package com.sandeep.RESTFW.RequestPOJOClasses;

public class Metadata
{
    public Metadata(String order_id)
    {
        this.order_id = order_id;
    }

    public Metadata()
    {

    }

    private String order_id;

    public String getOrder_id()
    {
        return order_id;
    }

    public void setOrder_id(String order_id)
    {
        this.order_id = order_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [order_id = " + order_id + "]";
    }
}
