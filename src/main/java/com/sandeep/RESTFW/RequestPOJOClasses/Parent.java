package com.sandeep.RESTFW.RequestPOJOClasses;

public class Parent
{
    private String key;

    public Parent(String key)
    {
        super();
        this.key = key;
    }

    public Parent()
    {
        // TODO Auto-generated constructor stub
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [key = " + key + "]";
    }
}
