package com.sandeep.RESTFW.RequestPOJOClasses;

public class PayPalPaymentRequest
{
    private Redirect_urls redirect_urls;

    private Transactions[] transactions;

    private String intent;

    private Payer payer;

    private String note_to_payer;

    public Redirect_urls getRedirect_urls()
    {
        return redirect_urls;
    }

    public void setRedirect_urls(Redirect_urls redirect_urls)
    {
        this.redirect_urls = redirect_urls;
    }

    public Transactions[] getTransactions()
    {
        return transactions;
    }

    public void setTransactions(Transactions[] transactions)
    {
        this.transactions = transactions;
    }

    public String getIntent()
    {
        return intent;
    }

    public void setIntent(String intent)
    {
        this.intent = intent;
    }

    public Payer getPayer()
    {
        return payer;
    }

    public void setPayer(Payer payer)
    {
        this.payer = payer;
    }

    public String getNote_to_payer()
    {
        return note_to_payer;
    }

    public void setNote_to_payer(String note_to_payer)
    {
        this.note_to_payer = note_to_payer;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [redirect_urls = " + redirect_urls + ", transactions = " + transactions + ", intent = "
            + intent + ", payer = " + payer + ", note_to_payer = " + note_to_payer + "]";
    }
}
