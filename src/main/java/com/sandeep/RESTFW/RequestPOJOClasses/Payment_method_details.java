package com.sandeep.RESTFW.RequestPOJOClasses;

public class Payment_method_details
{
    private String type;

    private Card card;

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Card getCard()
    {
        return card;
    }

    public void setCard(Card card)
    {
        this.card = card;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [type = " + type + ", card = " + card + "]";
    }
}
