package com.sandeep.RESTFW.RequestPOJOClasses;

public class ProductRequest
{
    private String price;

    private String name;

    private String upc;

    private String description;

    private String model;

    private String type;

    public String getPrice()
    {
        return price;
    }

    public void setPrice(String price)
    {
        this.price = price;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getUpc()
    {
        return upc;
    }

    public void setUpc(String upc)
    {
        this.upc = upc;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [price = " + price + ", name = " + name + ", upc = " + upc + ", description = " + description
            + ", model = " + model + ", type = " + type + "]";
    }
}
