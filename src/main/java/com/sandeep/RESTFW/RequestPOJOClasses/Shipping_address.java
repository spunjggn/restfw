package com.sandeep.RESTFW.RequestPOJOClasses;

public class Shipping_address
{
    private String country_code;

    private String city;

    private String phone;

    private String state;

    private String recipient_name;

    private String postal_code;

    private String line2;

    private String line1;

    public String getCountry_code()
    {
        return country_code;
    }

    public void setCountry_code(String country_code)
    {
        this.country_code = country_code;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getRecipient_name()
    {
        return recipient_name;
    }

    public void setRecipient_name(String recipient_name)
    {
        this.recipient_name = recipient_name;
    }

    public String getPostal_code()
    {
        return postal_code;
    }

    public void setPostal_code(String postal_code)
    {
        this.postal_code = postal_code;
    }

    public String getLine2()
    {
        return line2;
    }

    public void setLine2(String line2)
    {
        this.line2 = line2;
    }

    public String getLine1()
    {
        return line1;
    }

    public void setLine1(String line1)
    {
        this.line1 = line1;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [country_code = " + country_code + ", city = " + city + ", phone = " + phone + ", state = "
            + state + ", recipient_name = " + recipient_name + ", postal_code = " + postal_code + ", line2 = " + line2
            + ", line1 = " + line1 + "]";
    }
}
