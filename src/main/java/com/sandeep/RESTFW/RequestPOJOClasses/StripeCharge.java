package com.sandeep.RESTFW.RequestPOJOClasses;

import com.sandeep.RESTFW.POJOClasses.Refunds;

public class StripeCharge
{
    private String balance_transaction;

    private Billing_details billing_details;

    private Metadata metadata;

    private String livemode;

    private String description;

    private String failure_message;

    private String fraud_details;

    private String amount_refunded;

    private Refunds refunds;

    private String statement_descriptor;

    private String transfer_data;

    private String receipt_url;

    private String shipping;

    private String review;

    private String captured;

    private String calculated_statement_descriptor;

    private String currency;

    private String refunded;

    private String id;

    private String outcome;

    private String payment_method;

    private String order;

    private String amount;

    private String disputed;

    private String failure_code;

    private String transfer_group;

    private String on_behalf_of;

    private String created;

    private Payment_method_details payment_method_details;

    private String source_transfer;

    private String receipt_number;

    private String application;

    private String receipt_email;

    private String paid;

    private String application_fee;

    private String payment_intent;

    private String invoice;

    private String statement_descriptor_suffix;

    private String application_fee_amount;

    private String object;

    private String customer;

    private String status;

    public String getBalance_transaction()
    {
        return balance_transaction;
    }

    public void setBalance_transaction(String balance_transaction)
    {
        this.balance_transaction = balance_transaction;
    }

    public Billing_details getBilling_details()
    {
        return billing_details;
    }

    public void setBilling_details(Billing_details billing_details)
    {
        this.billing_details = billing_details;
    }

    public Metadata getMetadata()
    {
        return metadata;
    }

    public void setMetadata(Metadata metadata)
    {
        this.metadata = metadata;
    }

    public String getLivemode()
    {
        return livemode;
    }

    public void setLivemode(String livemode)
    {
        this.livemode = livemode;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getFailure_message()
    {
        return failure_message;
    }

    public void setFailure_message(String failure_message)
    {
        this.failure_message = failure_message;
    }

    public String getFraud_details()
    {
        return fraud_details;
    }

    public void setFraud_details(String fraud_details)
    {
        this.fraud_details = fraud_details;
    }

    public String getAmount_refunded()
    {
        return amount_refunded;
    }

    public void setAmount_refunded(String amount_refunded)
    {
        this.amount_refunded = amount_refunded;
    }

    public Refunds getRefunds()
    {
        return refunds;
    }

    public void setRefunds(Refunds refunds)
    {
        this.refunds = refunds;
    }

    public String getStatement_descriptor()
    {
        return statement_descriptor;
    }

    public void setStatement_descriptor(String statement_descriptor)
    {
        this.statement_descriptor = statement_descriptor;
    }

    public String getTransfer_data()
    {
        return transfer_data;
    }

    public void setTransfer_data(String transfer_data)
    {
        this.transfer_data = transfer_data;
    }

    public String getReceipt_url()
    {
        return receipt_url;
    }

    public void setReceipt_url(String receipt_url)
    {
        this.receipt_url = receipt_url;
    }

    public String getShipping()
    {
        return shipping;
    }

    public void setShipping(String shipping)
    {
        this.shipping = shipping;
    }

    public String getReview()
    {
        return review;
    }

    public void setReview(String review)
    {
        this.review = review;
    }

    public String getCaptured()
    {
        return captured;
    }

    public void setCaptured(String captured)
    {
        this.captured = captured;
    }

    public String getCalculated_statement_descriptor()
    {
        return calculated_statement_descriptor;
    }

    public void setCalculated_statement_descriptor(String calculated_statement_descriptor)
    {
        this.calculated_statement_descriptor = calculated_statement_descriptor;
    }

    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }

    public String getRefunded()
    {
        return refunded;
    }

    public void setRefunded(String refunded)
    {
        this.refunded = refunded;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getOutcome()
    {
        return outcome;
    }

    public void setOutcome(String outcome)
    {
        this.outcome = outcome;
    }

    public String getPayment_method()
    {
        return payment_method;
    }

    public void setPayment_method(String payment_method)
    {
        this.payment_method = payment_method;
    }

    public String getOrder()
    {
        return order;
    }

    public void setOrder(String order)
    {
        this.order = order;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }

    public String getDisputed()
    {
        return disputed;
    }

    public void setDisputed(String disputed)
    {
        this.disputed = disputed;
    }

    public String getFailure_code()
    {
        return failure_code;
    }

    public void setFailure_code(String failure_code)
    {
        this.failure_code = failure_code;
    }

    public String getTransfer_group()
    {
        return transfer_group;
    }

    public void setTransfer_group(String transfer_group)
    {
        this.transfer_group = transfer_group;
    }

    public String getOn_behalf_of()
    {
        return on_behalf_of;
    }

    public void setOn_behalf_of(String on_behalf_of)
    {
        this.on_behalf_of = on_behalf_of;
    }

    public String getCreated()
    {
        return created;
    }

    public void setCreated(String created)
    {
        this.created = created;
    }

    public Payment_method_details getPayment_method_details()
    {
        return payment_method_details;
    }

    public void setPayment_method_details(Payment_method_details payment_method_details)
    {
        this.payment_method_details = payment_method_details;
    }

    public String getSource_transfer()
    {
        return source_transfer;
    }

    public void setSource_transfer(String source_transfer)
    {
        this.source_transfer = source_transfer;
    }

    public String getReceipt_number()
    {
        return receipt_number;
    }

    public void setReceipt_number(String receipt_number)
    {
        this.receipt_number = receipt_number;
    }

    public String getApplication()
    {
        return application;
    }

    public void setApplication(String application)
    {
        this.application = application;
    }

    public String getReceipt_email()
    {
        return receipt_email;
    }

    public void setReceipt_email(String receipt_email)
    {
        this.receipt_email = receipt_email;
    }

    public String getPaid()
    {
        return paid;
    }

    public void setPaid(String paid)
    {
        this.paid = paid;
    }

    public String getApplication_fee()
    {
        return application_fee;
    }

    public void setApplication_fee(String application_fee)
    {
        this.application_fee = application_fee;
    }

    public String getPayment_intent()
    {
        return payment_intent;
    }

    public void setPayment_intent(String payment_intent)
    {
        this.payment_intent = payment_intent;
    }

    public String getInvoice()
    {
        return invoice;
    }

    public void setInvoice(String invoice)
    {
        this.invoice = invoice;
    }

    public String getStatement_descriptor_suffix()
    {
        return statement_descriptor_suffix;
    }

    public void setStatement_descriptor_suffix(String statement_descriptor_suffix)
    {
        this.statement_descriptor_suffix = statement_descriptor_suffix;
    }

    public String getApplication_fee_amount()
    {
        return application_fee_amount;
    }

    public void setApplication_fee_amount(String application_fee_amount)
    {
        this.application_fee_amount = application_fee_amount;
    }

    public String getObject()
    {
        return object;
    }

    public void setObject(String object)
    {
        this.object = object;
    }

    public String getCustomer()
    {
        return customer;
    }

    public void setCustomer(String customer)
    {
        this.customer = customer;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [balance_transaction = " + balance_transaction + ", billing_details = " + billing_details
            + ", metadata = " + metadata + ", livemode = " + livemode + ", description = " + description
            + ", failure_message = " + failure_message + ", fraud_details = " + fraud_details + ", amount_refunded = "
            + amount_refunded + ", refunds = " + refunds + ", statement_descriptor = " + statement_descriptor
            + ", transfer_data = " + transfer_data + ", receipt_url = " + receipt_url + ", shipping = " + shipping
            + ", review = " + review + ", captured = " + captured + ", calculated_statement_descriptor = "
            + calculated_statement_descriptor + ", currency = " + currency + ", refunded = " + refunded + ", id = " + id
            + ", outcome = " + outcome + ", payment_method = " + payment_method + ", order = " + order + ", amount = "
            + amount + ", disputed = " + disputed + ", failure_code = " + failure_code + ", transfer_group = "
            + transfer_group + ", on_behalf_of = " + on_behalf_of + ", created = " + created
            + ", payment_method_details = " + payment_method_details + ", source_transfer = " + source_transfer
            + ", receipt_number = " + receipt_number + ", application = " + application + ", receipt_email = "
            + receipt_email + ", paid = " + paid + ", application_fee = " + application_fee + ", payment_intent = "
            + payment_intent + ", invoice = " + invoice + ", statement_descriptor_suffix = "
            + statement_descriptor_suffix + ", application_fee_amount = " + application_fee_amount + ", object = "
            + object + ", customer = " + customer + ", status = " + status + "]";
    }
}
