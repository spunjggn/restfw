package com.sandeep.RESTFW.ResponsePOJOClasses;

public class BestBuyError
{
    private String code;

    BestBuyError()
    {

    }

    public BestBuyError(String code, Data data, String name, String className, String message, String[] errors)
    {
        super();
        this.code = code;
        this.data = data;
        this.name = name;
        this.className = className;
        this.message = message;
        this.errors = errors;
    }

    private Data data;

    private String name;

    private String className;

    private String message;

    private String[] errors;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public Data getData()
    {
        return data;
    }

    public void setData(Data data)
    {
        this.data = data;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getClassName()
    {
        return className;
    }

    public void setClassName(String className)
    {
        this.className = className;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String[] getErrors()
    {
        return errors;
    }

    public void setErrors(String[] errors)
    {
        this.errors = errors;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [code = " + code + ", data = " + data + ", name = " + name + ", className = " + className
            + ", message = " + message + ", errors = " + errors + "]";
    }
}
