package com.sandeep.RESTFW.ResponsePOJOClasses;

public class Error
{
    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    private String message;

    private String type;

    private String code;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDoc_url()
    {
        return doc_url;
    }

    public void setDoc_url(String doc_url)
    {
        this.doc_url = doc_url;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }

    private String doc_url;

    private String param;

}
