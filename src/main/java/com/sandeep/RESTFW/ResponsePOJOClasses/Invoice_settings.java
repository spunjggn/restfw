package com.sandeep.RESTFW.ResponsePOJOClasses;

public class Invoice_settings
{
    private String footer;

    private String custom_fields;

    private String default_payment_method;

    public String getFooter()
    {
        return footer;
    }

    public void setFooter(String footer)
    {
        this.footer = footer;
    }

    public String

        getCustom_fields()
    {
        return custom_fields;
    }

    public void setCustom_fields(String custom_fields)
    {
        this.custom_fields = custom_fields;
    }

    public String

        getDefault_payment_method()
    {
        return default_payment_method;
    }

    public void setDefault_payment_method(String default_payment_method)
    {
        this.default_payment_method = default_payment_method;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [footer = " + footer + ", custom_fields = " + custom_fields + ", default_payment_method = "
            + default_payment_method + "]";
    }
}
