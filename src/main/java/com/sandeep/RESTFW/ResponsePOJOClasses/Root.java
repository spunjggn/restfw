package com.sandeep.RESTFW.ResponsePOJOClasses;

public class Root
{
    private Error error;

    public Error getError()
    {
        return error;
    }

    public void setError(Error error)
    {
        this.error = error;
    }

}
