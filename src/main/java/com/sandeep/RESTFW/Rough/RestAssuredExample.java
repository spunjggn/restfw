package com.sandeep.RESTFW.Rough;

import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

import io.restassured.response.Response;

public class RestAssuredExample
{

    // public static ConfigProperties config;
    //
    // @BeforeMethod
    // public void beforemethod()
    // {
    //
    // ConfigFactory.setProperty("environment", "Stage");
    // config = ConfigFactory.create(ConfigProperties.class);
    // System.out.println(""+config.getbasePath());
    // System.out.println(""+config.getbaseURI());
    // RestAssured.baseURI="";
    // RestAssured.basePath="";
    //
    // }
    //

    @Test(enabled = false)
    public void createACustomer()
    {

        Response response = given().auth().basic("sk_test_ioU0sh31xy8x56htaaZqLsQ1", "")
            .formParam("email", "rahul111@gmail.com").formParam("description", "Added this customer using restasurred")
            .post("/customers").then().extract().response();

    }

    @Test
    public String generateToken()
    {
        Response response = given().contentType("x-www-form-urlencoded").auth()
            .basic("AaZZnMHxUI6D9RS-tgWeG3j-XcJlD28-1Z7xofP_3AAqtBkumvS4a4_gCcShmt8JG7AifWTdjlmdSX-4",
                "EO5UJTtnaeK295qTGoz89nkEC9hg9YkLZFxz8bNH4VRAEnCBKdA2mXhNVK8lGeYqfhKPEX1Y5eyOXeHi")
            .log().all().header("Content-Type", "application/x-www-form-urlencoded")
            .post("https://api.sandbox.paypal.com/v1/oauth2/token").then().extract().response();
        System.out.println(response.asString());
        String token = response.jsonPath().get("access_token");
        System.out.println("Token received for 2.0 Oauth is " + token);
        System.out.println("Hallo");
        return token;
    }

}
