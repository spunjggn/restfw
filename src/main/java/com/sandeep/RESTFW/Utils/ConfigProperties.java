package com.sandeep.RESTFW.Utils;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;

@Sources({"file:src\\test\\resources\\propertyFiles\\dev.properties" // mention the property file name ${enviroment}
})
public interface ConfigProperties extends Config
{

    @Key("baseURI")
    String getbaseURI();

    @Key("basePath")
    String getbasePath();

    @Key("username")
    String getusername();

    @Key("portNumber")
    int getPortNumber();

    @Key("SheetName")
    String getsheetname();

    @Key("secretKey")
    String getsecretKey();

    @Key("name")
    String getname();

    @Key("type")
    String gettype();

    @Key("price")
    float getprice();

    @Key("shipping")
    float getshipping();

    @Key("upc")
    String getupc();

    @Key("model")
    String getmodel();

    @Key("description")
    String getdescription();

    @Key("PayPalBaseURI")
    String getPayPalBaseURI();

    @Key("PayPalAuthPath")
    String getPayPalAuthPath();

    @Key("PayPalUser")
    String getPayPalUser();

    @Key("PaylPalPassword")
    String getPaylPalPassword();

    @Key("PayPalPayment")
    String getPayPalPayment();

    @Key("JiraURI")
    String getJiraURI();

    @Key("JiraBasePath")
    String getJiraBasePath();

    @Key("JiraIssueEndPoint")
    String getJiraIssueEndPoint();

    @Key("JiraCookieEndPoint")
    String getJiraCookieEndPoint();

    @Key("JiraIssueEndPointForAssignment")
    String getJiraIssueEndPointForAssignment();

    @Key("JiraIssueEndPointForComment")
    String getJiraIssueEndPointForComment();

}
