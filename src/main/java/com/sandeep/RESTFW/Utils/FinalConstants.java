package com.sandeep.RESTFW.Utils;

public class FinalConstants
{
    public static final String CREATECUSTOMER_PUBLIC_KEY =
        "sk_test_51H3hf8G1vWrroZEBLBS7lK3oVThSnY0hwBpkUvFDAkcBlK7d17p5ac37xjOXHedlRnxmmFrd50x3miFuORGxmkJ100EKkIMm26";

    public static final String CREATECUSTOMER_INVALID_PUBLIC_KEY =
        "sk_test_51H3hf8G1vWrroZEBLBS7lK3oVThSnY0hwBpkUvFDAkcBlK7d17p5ac37xjOXHedlRnxmmFrd50x3miFuORGxmkJ1";

    public static final String CREATECUSTOMER_DESC = "Ramdas";

    public static final String CREATECUSTOMER_NULL_DESC = null;

    public static final String PAYPAL_GRANT_TYPE = "client_credentials";

    public static final String JIRA_USER_NAME = "spunjggn";

    public static final String JIRA_PASSWORD = "Iaminggn122004";

    public static final String connString = "jdbc:mysql://localhost:3306/employeeportal";

    public static final String user = "root";

    public static final String password = "root";

    public static final String RETRIEVE_STRIPE_CUST = "cus_Hoe18QzAPSxPor";

    public static final String RETRIEVE_STRIPE_INVALID_CUST = "Hoe18QzAPSxPor";

    public static final String RETRIEVE_STRIPE_NULL_CUST = null;

    public static final String RETRIEVE_STRIPE_BLANK_CUST = " ";

    public static final String STRIPE_CHARGES_END_POINT = "charges";

    public static final int STRIPE_CHARGES_AMOUNT = 1000;

    public static final String STRIPE_CHARGES_CURRENCY = "USD";

    public static final String STRIPE_CHARGES_SOURCE = "tok_visa";

    public static final String STRIPE_CHARGES_DESCRIPTION = "TEST CHARGE";

    public static final String JIRA_ISSUE_TYPE = "Task";

    public static final String JIRA_PROJECT_NAME = "SPA";

    public static final String JIRA_ISSUE_SUMMARY = "This is a test Ticket";

    public static final String JIRA_ISSUE_DESCRIPTION = "We need to Record the Task assigned to us for the Automation";

    public static final String JIRA_ISSUE_PARENT = "SPA-4";

    public static final String JIRA_ASSIGNEE = "spunjggn";

    public static final String JIRA_UPDATED_COMMENT = "Updated comment from spunjggn";

    public static final String BESTBUY_NULL_NAME = "NULL";

    public static final String BESTBUY_BLANK_NAME = " ";

    public static final String BESTBUY_NULL_UPC = "NULL";

    public static final String BESTBUY_BLANK_UPC = " ";

    public static final float BESTBUY_ZERO_PRICE = 0.0f;

    public static final float BESTBUY_NEGATIVE_PRICE = -1.0f;

    public static final float BESTBUY_DECIMAL_PRICE = 1.1f;

}

class PaypalConstants

{

    public static final String CHECK_Paypal = "Testing123";

}
