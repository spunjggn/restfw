package com.sandeep.RESTFW.Utils;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestTemplate
{

    public static Response post(final RequestSpecification requestspecification)

    {
        return given().spec(requestspecification).when().log().all().post().then().extract().response();

    }

    public static Response get(final RequestSpecification requestspecification)
    {
        return given().spec(requestspecification).when().log().all().get().then().extract().response();

    }

    public static Response get(final RequestSpecification requestspecification, String endPoint)
    {

        return given().spec(requestspecification).when().log().all().get(endPoint).then().extract().response();

    }

    public static Response delete(RequestSpecification requestspecification, String endPoint)
    {
        return given().spec(requestspecification).when().log().all().delete(endPoint).then().extract().response();

    }

    public static Response post(final RequestSpecification requestspecification, String endPoint)

    {
        return given().spec(requestspecification).when().log().all().post(endPoint).then().extract().response();

    }

    public static Response put(RequestSpecification requestspecification, String endPoint)
    {
        return given().spec(requestspecification).when().log().all().put(endPoint).then().extract().response();
    }
}
