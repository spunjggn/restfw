package com.sandeep.RESTFW.Utils;

import org.testng.asserts.IAssert;

import com.aventstack.extentreports.Status;
import com.sandeep.RESTFW.Setup.TestSetup;

public class SoftAssert extends org.testng.asserts.SoftAssert
{

    @Override
    public void onAssertFailure(IAssert A, java.lang.AssertionError ax)
    {
        TestSetup.logAssertionDetails("Error Message -> " + ax.getMessage(), Status.FAIL);
    }

    @Override
    public void onAssertSuccess(IAssert<?> assertCommand)
    {
        TestSetup.logAssertionDetails(
            "Actual -> " + assertCommand.getActual() + " , " + " Expected -> " + assertCommand.getExpected(),
            Status.PASS);
    }

}
