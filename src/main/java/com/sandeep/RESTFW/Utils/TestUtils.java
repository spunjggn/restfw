package com.sandeep.RESTFW.Utils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.io.filefilter.WildcardFilter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestUtils
{

    public static int getExpectedStatusCode(String expectedStatusCode)
    {
        return Integer.parseInt(expectedStatusCode);
    }

    public static void renameReport()
    {
        TimeZone.setDefault(TimeZone.getTimeZone("IST"));
        SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String timestamp = f.format(new Date());
        // newFileName = newFileName + timestamp+ ".html";
        File file = new File("./extentreport.html");
        file.renameTo(new File("./src/test/resources/testReport/extentreport_" + timestamp + ".html"));

    }

    /*
     * Commented as updating the current code public static void moveReportToAchive() { String newFileName="TestReport";
     * TimeZone.setDefault(TimeZone.getTimeZone("IST")); SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd_HHmmss");
     * String timestamp =f.format(new Date()); newFileName = newFileName + timestamp+ ".html"; File file =new
     * File("./extentreport.html"); if (file.renameTo(new File("./src/test/resources/archivedReports/"+newFileName))) {
     * System.out.println("File has been moved"); file.delete(); } else { System.out.println("File has not been moved");
     * } }
     */

    // public static void moveReportToAchive()
    // {
    // File file = new File("./src/test/resources/testReport/extentreport_.html");
    // if (file.exists()) {
    // file.renameTo(new File("./src/test/resources/archivedReports/"));
    // System.out.println("File has been moved");
    // file.delete();
    // } else {
    // System.out.println("File has not been moved");
    // }
    //
    // }

    // public static void copyFile() throws IOException
    // {
    //
    // file.get
    // Path sourcePath = Paths.get("./src/test/resources/testReport/extentReport*.html");
    // Path destinationPath = Paths.get("./src/test/resources/archivedReports/");
    // try {
    // Files.move(sourcePath, destinationPath, StandardCopyOption.REPLACE_EXISTING);
    // } catch (IOException e) {
    // System.out.println("Moving file failed");
    // e.printStackTrace();
    // }
    // }

    public static void moveReportToAchive()
    {
        File[] files = new File("./src/test/resources/testReport/")
            .listFiles((FileFilter) new WildcardFilter("extentreport*.html"));
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            String filename = file.getName();
            if (file.exists()) {
                file.renameTo(new File("./src/test/resources/archivedReports/" + filename));
                System.out.println("Files has been moved");
                file.delete();
            } else {
                System.out.println("Files has not been moved");
            }
        }
    }

    // This also works if we have only one file to be moved in TestReports folder

    // public static void moveReportToAchive()
    // {
    // File[] files = new File("./src/test/resources/testReport/")
    // .listFiles((FileFilter) new WildcardFilter("extentreport*.html"));
    // File file = files[0];
    // String filename = file.getName();
    // if (file.exists()) {
    // file.renameTo(new File("./src/test/resources/archivedReports/" + filename));
    // System.out.println("Files has been moved");
    // file.delete();
    // } else {
    // System.out.println("Files has not been moved");
    // }
    // }

    public static <T> Object mappermethod1(String resp, Class<T> type)
        throws JsonParseException, JsonMappingException, IOException
    {
        Class[] classObjectArray;
        Class classObject;
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootnode = mapper.readTree(resp);
        if (rootnode.isObject()) {
            classObject = mapper.readValue(resp, Class.class);
            return classObject;
        } else if (rootnode.isArray()) {
            classObjectArray = mapper.readValue(resp, Class[].class);
            return classObjectArray;
        } else {
            return null;
        }

    }

    public static <T> Object mappermethod(String resp, Class<T> type)
        throws JsonParseException, JsonMappingException, IOException
    {

        Class classObject;
        ObjectMapper mapper = new ObjectMapper();
        // JsonNode rootnode = mapper.readTree(resp);

        classObject = mapper.readValue(resp, Class.class);
        return classObject;

    }

}
