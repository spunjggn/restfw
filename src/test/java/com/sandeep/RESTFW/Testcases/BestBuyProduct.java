package com.sandeep.RESTFW.Testcases;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sandeep.RESTFW.ResponsePOJOClasses.BestBuyError;
import com.sandeep.RESTFW.ResponsePOJOClasses.ProductResponse;
import com.sandeep.RESTFW.Setup.TestSetup;
import com.sandeep.RESTFW.Utils.FinalConstants;

import APIHelper.BestBuyProductAPI;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class BestBuyProduct extends TestSetup {
    Response response;

    /*
     * These test cases helped with understanding following concepts Overridden custom asserts Mapper I have created a
     * body object here and passed API is from BestBuy
     */

    DecimalFormat df = new DecimalFormat(".0");

    @BeforeTest()
    public static void seturl() {
        RestAssured.baseURI = "http://localhost:3030";
        RestAssured.basePath = "/products";
    }

    @Test(enabled = false, priority = 1)
    public void createProductPositiveFlow() throws JsonParseException, JsonMappingException, IOException {
        response = BestBuyProductAPI.BestBuyCreateProduct(configProperties.getname(), configProperties.gettype(),
            configProperties.getprice(), configProperties.getupc(), configProperties.getdescription(),
            configProperties.getmodel());
        ObjectMapper mapper = new ObjectMapper();
        ProductResponse resp = mapper.readValue(response.asString(), ProductResponse.class);
        // ProductResponse resp = (ProductResponse) TestUtils.mappermethod(response.asString(), ProductResponse.class);
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_CREATED);
        softassertion.assertEquals(resp.getName(), "Lungi");
        softassertion.assertNotNull(resp.getPrice());
        softassertion.assertAll();
    }

    @Test(enabled = false, priority = 2)
    public void createProductwithNullname() throws JsonParseException, JsonMappingException, IOException {
        response = BestBuyProductAPI.BestBuyCreateProduct(FinalConstants.BESTBUY_NULL_NAME, configProperties.gettype(),
            configProperties.getprice(), configProperties.getupc(), configProperties.getdescription(),
            configProperties.getmodel());
        ObjectMapper mapper = new ObjectMapper();
        ProductResponse resp = mapper.readValue(response.asString(), ProductResponse.class);
        // ProductResponse resp = (ProductResponse) TestUtils.mappermethod(response.asString(), ProductResponse.class);
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_CREATED);
        softassertion.assertEquals(resp.getName(), FinalConstants.BESTBUY_NULL_NAME);
        softassertion.assertNotNull(resp.getPrice());

        // assertEquals(resp.getPrice(), configProperties.getprice());
        softassertion.assertAll();
    }

    @Test(enabled = false, priority = 3)
    public void createProductwithBlankname() throws JsonParseException, JsonMappingException, IOException {
        response = BestBuyProductAPI.BestBuyCreateProduct(FinalConstants.BESTBUY_BLANK_NAME, configProperties.gettype(),
            configProperties.getprice(), configProperties.getupc(), configProperties.getdescription(),
            configProperties.getmodel());
        ObjectMapper mapper = new ObjectMapper();
        ProductResponse resp = mapper.readValue(response.asString(), ProductResponse.class);
        // ProductResponse resp = (ProductResponse) TestUtils.mappermethod(response.asString(), ProductResponse.class);
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_CREATED);
        softassertion.assertEquals(resp.getName(), FinalConstants.BESTBUY_BLANK_NAME);
        softassertion.assertNotNull(resp.getPrice());
        softassertion.assertAll();
    }

    @Test(enabled = false, priority = 4)
    public void createProductWithNullUpc() throws JsonParseException, JsonMappingException, IOException {
        response = BestBuyProductAPI.BestBuyCreateProduct(configProperties.getname(), configProperties.gettype(),
            configProperties.getprice(), FinalConstants.BESTBUY_NULL_UPC, configProperties.getdescription(),
            configProperties.getmodel());
        ObjectMapper mapper = new ObjectMapper();
        ProductResponse resp = mapper.readValue(response.asString(), ProductResponse.class);
        // ProductResponse resp = (ProductResponse) TestUtils.mappermethod(response.asString(), ProductResponse.class);
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_CREATED);
        softassertion.assertEquals(resp.getUpc(), FinalConstants.BESTBUY_NULL_UPC);
        softassertion.assertNotNull(resp.getPrice());
        softassertion.assertAll();
    }

    @Test(enabled = false, priority = 5)
    public void createProductWithBlankUpc() throws JsonParseException, JsonMappingException, IOException {
        response = BestBuyProductAPI.BestBuyCreateProduct(configProperties.getname(), configProperties.gettype(),
            configProperties.getprice(), FinalConstants.BESTBUY_BLANK_UPC, configProperties.getdescription(),
            configProperties.getmodel());
        ObjectMapper mapper = new ObjectMapper();
        ProductResponse resp = mapper.readValue(response.asString(), ProductResponse.class);
        // ProductResponse resp = (ProductResponse) TestUtils.mappermethod(response.asString(), ProductResponse.class);
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_CREATED);
        softassertion.assertEquals(resp.getUpc(), FinalConstants.BESTBUY_BLANK_UPC);
        softassertion.assertNotNull(resp.getPrice());
        softassertion.assertAll();
    }

    @Test(enabled = false, priority = 7)
    public void createProductwithZeroPrice() throws JsonParseException, JsonMappingException, IOException {
        response = BestBuyProductAPI.BestBuyCreateProduct(configProperties.getname(), configProperties.gettype(),
            FinalConstants.BESTBUY_ZERO_PRICE, configProperties.getupc(), configProperties.getdescription(),
            configProperties.getmodel());
        ObjectMapper mapper = new ObjectMapper();
        ProductResponse resp = mapper.readValue(response.asString(), ProductResponse.class);
        // ProductResponse resp = (ProductResponse) TestUtils.mappermethod(response.asString(), ProductResponse.class);
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_CREATED);
        softassertion.assertEquals(resp.getName(), "Lungi");
        softassertion.assertEquals(resp.getPrice(), FinalConstants.BESTBUY_ZERO_PRICE);
        softassertion.assertAll();
    }

    @Test(enabled = false, priority = 8)
    public void createProductwithNegativePrice() throws JsonParseException, JsonMappingException, IOException {
        response = BestBuyProductAPI.BestBuyCreateProduct(configProperties.getname(), configProperties.gettype(),
            FinalConstants.BESTBUY_NEGATIVE_PRICE, configProperties.getupc(), configProperties.getdescription(),
            configProperties.getmodel());
        ObjectMapper mapper = new ObjectMapper();
        ProductResponse resp = mapper.readValue(response.asString(), ProductResponse.class);
        // ProductResponse resp = (ProductResponse) TestUtils.mappermethod(response.asString(), ProductResponse.class);
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_CREATED);
        softassertion.assertEquals(resp.getName(), "Lungi");
        softassertion.assertEquals(resp.getPrice(), FinalConstants.BESTBUY_NEGATIVE_PRICE);
        softassertion.assertAll();
    }

    @Test(enabled = false, priority = 6)
    public void createProductwithDecimalPrice() throws JsonParseException, JsonMappingException, IOException {
        float price = FinalConstants.BESTBUY_DECIMAL_PRICE;
        // float priceTobeUsed =(float)price;

        response = BestBuyProductAPI.BestBuyCreateProduct(configProperties.getname(), configProperties.gettype(), price,
            configProperties.getupc(), configProperties.getdescription(), configProperties.getmodel());

        ObjectMapper mapper = new ObjectMapper();
        ProductResponse resp = mapper.readValue(response.asString(), ProductResponse.class);
        // ProductResponse resp = (ProductResponse) TestUtils.mappermethod(response.asString(), ProductResponse.class);
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_CREATED);
        softassertion.assertEquals(resp.getName(), "Lungi");

        // softassertion.assertEquals(resp.getPrice(), 10.1f, 0.0f);
        softassertion.assertEquals(df.format(resp.getPrice()), df.format(FinalConstants.BESTBUY_DECIMAL_PRICE));
        softassertion.assertAll();
    }

    @Test(enabled = true, priority = 9)
    public void createProductwithAllNullVal() throws JsonParseException, JsonMappingException, IOException {
        response = BestBuyProductAPI.BestBuyCreateProduct();
        ObjectMapper mapper = new ObjectMapper();
        BestBuyError err = mapper.readValue(response.asString(), BestBuyError.class);
        // ProductResponse resp = (ProductResponse) TestUtils.mappermethod(response.asString(), ProductResponse.class);
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_BAD_REQUEST);

        softassertion.assertEquals(err.getMessage(), "Invalid Parameters");

        softassertion.assertEquals(err.getCode(), "400");

        ArrayList list = new ArrayList();
        list.add("'name' should be string");
        list.add("'type' should be string");
        list.add("'price' should be number");
        list.add("'upc' should be string");
        list.add("'description' should be string");
        list.add("'model' should be string");

        String[] array = err.getErrors();
        for (int i = 0; i < err.getErrors().length; i++) {
            // System.out.println(array[i]);
            // System.out.println(list.get(i));
            softassertion.assertEquals(array[i], list.get(i));
        }

        softassertion.assertAll();

    }
}
