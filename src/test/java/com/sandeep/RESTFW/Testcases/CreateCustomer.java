package com.sandeep.RESTFW.Testcases;

import static com.sandeep.RESTFW.Utils.FinalConstants.CREATECUSTOMER_DESC;
import static com.sandeep.RESTFW.Utils.FinalConstants.CREATECUSTOMER_INVALID_PUBLIC_KEY;
import static com.sandeep.RESTFW.Utils.FinalConstants.CREATECUSTOMER_NULL_DESC;
import static com.sandeep.RESTFW.Utils.FinalConstants.CREATECUSTOMER_PUBLIC_KEY;
import static com.sandeep.RESTFW.Utils.FinalConstants.RETRIEVE_STRIPE_BLANK_CUST;
import static com.sandeep.RESTFW.Utils.FinalConstants.RETRIEVE_STRIPE_CUST;
import static com.sandeep.RESTFW.Utils.FinalConstants.RETRIEVE_STRIPE_INVALID_CUST;
import static com.sandeep.RESTFW.Utils.FinalConstants.RETRIEVE_STRIPE_NULL_CUST;

import java.io.IOException;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sandeep.RESTFW.ResponsePOJOClasses.CustomerResponse;
import com.sandeep.RESTFW.ResponsePOJOClasses.Root;
import com.sandeep.RESTFW.Setup.TestSetup;

import APIHelper.StripeCreateCustomerAPI;
import io.restassured.response.Response;

public class CreateCustomer extends TestSetup
{

    Response resp;

    /*
     * This API is from Stripe and helps understand the basic oauth and framework,POST method
     */

    public static String createCustomerFlowfordeletion() throws JsonParseException, JsonMappingException, IOException
    {

        Response resp = StripeCreateCustomerAPI.postCreateCustomerAPI(CREATECUSTOMER_PUBLIC_KEY, CREATECUSTOMER_DESC);
        TestSetup.logDetails(resp.asString(), "response");
        ObjectMapper mapper = new ObjectMapper();
        CustomerResponse customer = mapper.readValue(resp.asString(), CustomerResponse.class);

        return customer.getId();

    }

    /**
     * @author Sandeep Punj
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @Description This is a positive flow to test the creation of Customer using valid Key and formparam description.
     */
    @Test(priority = 1, groups = "Sanity")
    public static void createCustomerFlow() throws JsonParseException, JsonMappingException, IOException
    {

        Response resp = StripeCreateCustomerAPI.postCreateCustomerAPI(CREATECUSTOMER_PUBLIC_KEY, CREATECUSTOMER_DESC);
        TestSetup.logDetails(resp.asString(), "response");
        ObjectMapper mapper = new ObjectMapper();
        CustomerResponse customer = mapper.readValue(resp.asString(), CustomerResponse.class);
        softassertion.assertNotNull(customer.getId());
        softassertion.assertNotNull(customer.getSources().getUrl());
        softassertion.assertEquals(resp.statusCode(), HttpStatus.SC_OK);
        softassertion.assertEquals(customer.getDescription(), CREATECUSTOMER_DESC);
        softassertion.assertAll();

    }

    @Test(priority = 2)
    public void createCustomerFlowWithInvalidKey() throws JsonParseException, JsonMappingException, IOException
    {

        Response resp =
            StripeCreateCustomerAPI.postCreateCustomerAPI(CREATECUSTOMER_INVALID_PUBLIC_KEY, CREATECUSTOMER_DESC);
        TestSetup.logDetails(resp.asString(), "response");
        ObjectMapper mapper = new ObjectMapper();
        Root errorRoot = mapper.readValue(resp.asString(), Root.class);
        softassertion.assertNotNull(errorRoot.getError().getMessage());
        softassertion.assertEquals(errorRoot.getError().getType(), "invalid_request_error");
        softassertion.assertEquals(resp.statusCode(), HttpStatus.SC_UNAUTHORIZED);
        softassertion.assertAll();

    }

    @Test(priority = 3)
    public void createCustomerFlowWithNullDesc() throws JsonParseException, JsonMappingException, IOException
    {

        Response resp =
            StripeCreateCustomerAPI.postCreateCustomerAPI(CREATECUSTOMER_PUBLIC_KEY, CREATECUSTOMER_NULL_DESC);
        TestSetup.logDetails(resp.asString(), "response");
        ObjectMapper mapper = new ObjectMapper();

        CustomerResponse customer = mapper.readValue(resp.asString(), CustomerResponse.class);
        softassertion.assertNotNull(customer.getId());
        softassertion.assertNull(customer.getDescription());
        softassertion.assertNotNull(customer.getSources().getUrl());
        softassertion.assertEquals(resp.statusCode(), HttpStatus.SC_OK);
        softassertion.assertAll();

    }

    @Test(priority = 4, groups = "Sanity")
    public void RetriveCustomerWithValidCustid() throws JsonParseException, JsonMappingException, IOException
    {
        resp = StripeCreateCustomerAPI.getCustomerAPI(CREATECUSTOMER_PUBLIC_KEY, RETRIEVE_STRIPE_CUST);
        TestSetup.logDetails(resp.asString(), "response");
        ObjectMapper mapper = new ObjectMapper();
        CustomerResponse customer = mapper.readValue(resp.asString(), CustomerResponse.class);
        softassertion.assertEquals(resp.statusCode(), HttpStatus.SC_OK);
        softassertion.assertEquals(customer.getId(), RETRIEVE_STRIPE_CUST);
        softassertion.assertNotNull(customer.getDescription());
        softassertion.assertAll();
    }

    @Test(priority = 5, enabled = true)
    public void RetriveCustomerWithInValidCustid() throws JsonParseException, JsonMappingException, IOException
    {
        Response resp = StripeCreateCustomerAPI.getCustomerAPI(CREATECUSTOMER_PUBLIC_KEY, RETRIEVE_STRIPE_INVALID_CUST);
        TestSetup.logDetails(resp.asString(), "response");
        ObjectMapper mapper = new ObjectMapper();
        Root errorRoot = mapper.readValue(resp.asString(), Root.class);
        softassertion.assertEquals(errorRoot.getError().getMessage().substring(0, 16), "No such customer");
        softassertion.assertEquals(errorRoot.getError().getType(), "invalid_request_error");
        softassertion.assertEquals(resp.statusCode(), HttpStatus.SC_NOT_FOUND);
        softassertion.assertAll();
    }

    @Test(priority = 6, enabled = true)
    public void RetriveCustomerWithNullCustid() throws JsonParseException, JsonMappingException, IOException
    {
        try {
            resp = StripeCreateCustomerAPI.getCustomerAPI(CREATECUSTOMER_PUBLIC_KEY, RETRIEVE_STRIPE_NULL_CUST);
        } catch (IllegalArgumentException ae) {
            System.out.println("The null path param cannot be sent");
            ae.printStackTrace();
            Logger().debug(ae);
            softassertion.assertTrue(true);
            softassertion.assertAll();
        }

    }

    @Test(priority = 7, enabled = true)
    public void RetriveCustomerBlankCustid() throws JsonParseException, JsonMappingException, IOException
    {

        Response resp = StripeCreateCustomerAPI.getCustomerAPI(CREATECUSTOMER_PUBLIC_KEY, RETRIEVE_STRIPE_BLANK_CUST);
        TestSetup.logDetails(resp.asString(), "response");
        ObjectMapper mapper = new ObjectMapper();
        Root errorRoot = mapper.readValue(resp.asString(), Root.class);
        System.out.println(errorRoot.getError().getMessage().substring(0, 47));
        softassertion.assertEquals(errorRoot.getError().getMessage().substring(0, 46),
            "Unrecognized request URL (GET: /v1/customers/)");
        TestSetup.logDetails(errorRoot.getError().getMessage(), "ErrorMessage");
        softassertion.assertEquals(errorRoot.getError().getType(), "invalid_request_error");
        softassertion.assertAll();
    }

}
