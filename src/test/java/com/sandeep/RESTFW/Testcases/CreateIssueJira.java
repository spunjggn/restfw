package com.sandeep.RESTFW.Testcases;

import java.io.IOException;

import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sandeep.RESTFW.RequestPOJOClasses.Fields;
import com.sandeep.RESTFW.RequestPOJOClasses.Issue;
import com.sandeep.RESTFW.RequestPOJOClasses.Issuetype;
import com.sandeep.RESTFW.RequestPOJOClasses.Parent;
import com.sandeep.RESTFW.RequestPOJOClasses.Project;
import com.sandeep.RESTFW.Setup.TestSetup;
import com.sandeep.RESTFW.Utils.FinalConstants;

import APIHelper.JiraHelperAPI;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class CreateIssueJira extends TestSetup
{
    /*
     * This is a demonstration of Cookie based Authentication for Jira Tools Jira is installed locally on my machine
     * with port 8070 http://localhost:8070/ The example uses Jsession Cokiee and is recieved in response of Username
     * and Password It also shows chaining concept for maintaining the session(Output of one api as input of another
     * api) Few other basics learned. code Beautify to convert Json payload to JsonString Payload Eclipse sometime
     * handles Json into string by itself by just copying it Json object as a Map using dependency
     * com.googlecode.json-simple For API reference
     * https://developer.atlassian.com/server/jira/platform/cookie-based-authentication/
     * https://developer.atlassian.com/server/jira/platform/jira-rest-api-examples/
     */

    static Response response;

    @BeforeTest()
    public static void beforetest()
    {
        RestAssured.baseURI = configProperties.getJiraURI();
        RestAssured.basePath = configProperties.getJiraBasePath();
    }

    @Test(enabled = true, priority = 1)

    public static void createtask() throws JsonParseException, JsonMappingException, IOException
    {

        response = JiraHelperAPI.createIssueAPI(FinalConstants.JIRA_ISSUE_SUMMARY,
            FinalConstants.JIRA_ISSUE_DESCRIPTION, FinalConstants.JIRA_ISSUE_TYPE, FinalConstants.JIRA_PROJECT_NAME);

        TestSetup.logDetails(response.asString(), "Response");
        System.out.println(response.asString());
        System.out.println(response.statusCode());
        softassertion.assertEquals(response.statusCode(), HttpStatus.SC_CREATED);
        softassertion.assertAll();

    }

    @Test(enabled = false)
    /**
     * @description This test case is not currently working
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */

    public static void createsubtask() throws JsonParseException, JsonMappingException, IOException
    {

        // response =
        // JiraHelperAPI.createIssueAPI(FinalConstants.JIRA_ISSUE_SUMMARY, FinalConstants.JIRA_ISSUE_DESCRIPTION,
        // FinalConstants.JIRA_ISSUE_TYPE, FinalConstants.JIRA_PROJECT_NAME, FinalConstants.JIRA_ISSUE_PARENT);
        //
        // System.out.println(response.getStatusCode());
        // System.out.println(response.asString());

        String getCookie = JiraHelperAPI.generateCookie().cookies().get("JSESSIONID");

        ObjectMapper objmapper = new ObjectMapper();
        Issuetype issuetype = new Issuetype("Task");
        Project project = new Project("SPA");
        Parent parent = new Parent("SPA-1");
        // issuetype.setId(5);

        Fields fields =
            new Fields("My Getter Setter", issuetype, project, "I am using POJO to make the body of request", parent);

        Issue issue = new Issue(fields);

        System.out.println("" + issue.toString());

        Response response = RestAssured.given().header("Content-Type", "application/json")
            .cookie("JSESSIONID", getCookie).body(issue).post("http://localhost:8070/rest/api/2/issue").andReturn();
        System.out.println(response.getStatusCode());
        System.out.println(response.asString());

    }

    @Test(enabled = true, priority = 2)

    public static void assignTheTask() throws JsonParseException, JsonMappingException, IOException
    {
        response = JiraHelperAPI.assigntaskAPI();
        TestSetup.logDetails(response.asString(), "Response");

        softassertion.assertEquals(response.getStatusCode(), HttpStatus.SC_NO_CONTENT);
        softassertion.assertAll();
    }

    @Test(enabled = true, priority = 3)

    public static void addingAComponent()
    {
        String getCookie = JiraHelperAPI.generateCookie().cookies().get("JSESSIONID");
        Response response = RestAssured.given().header("Content-Type", "application/json")
            .cookie("JSESSIONID", getCookie)
            .body(
                "{\r\n   \"update\" : {\r\n       \"components\" : [{\"add\" : {\"name\" : \"Engine\"}}]\r\n   }\r\n}")
            .put("http://localhost:8070/rest/api/2/issue/SPA-2").andReturn();
        TestSetup.logDetails(response.asString(), "Response");

        softassertion.assertEquals(response.getStatusCode(), HttpStatus.SC_NO_CONTENT);
        softassertion.assertAll();
    }

    @Test(enabled = true, priority = 4, invocationCount = 5)

    public static void addingAComnent()
    {

        response = JiraHelperAPI.addingAComnentAPI();

        TestSetup.logDetails(response.asString(), "Response");

        softassertion.assertEquals(response.getStatusCode(), HttpStatus.SC_CREATED);
        softassertion.assertAll();
    }

}
