package com.sandeep.RESTFW.Testcases;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.sandeep.RESTFW.Setup.TestSetup;
import com.sandeep.RESTFW.Utils.DBQuery;

public class DbValidations extends TestSetup
{

    static String value;

    static ArrayList expectedlist = new ArrayList();

    @Test(enabled = true, priority = 1)
    public static void dbValidationforPersonCity() throws ClassNotFoundException, SQLException
    {

        String url = "\"jdbc:mysql://localhost:3306/employeeportal\",\"root\",\"root\"";

        dbh.setConnectionString(url);
        outerMap = dbh.executeQuery(DBQuery.db3);
        String Col = "FirstName";
        Logger().log(Status.INFO, "Query getting fired -->  " + DBQuery.db3);
        Logger().log(Status.INFO, "give the name of the column on which assert has to be applied -->  " + Col);

        List firstname = getDataforColumn(Col);
        System.out.println("" + firstname);
        softassertion.assertTrue(firstname.contains("Amit"));
        softassertion.assertAll();

    }

    @Test(enabled = true, priority = 2)
    public static void dbValidationforEmployees() throws ClassNotFoundException, SQLException
    {

        String url = "\"jdbc:mysql://localhost:3306/employeeportal\",\"root\",\"root\"";

        dbh.setConnectionString(url);
        outerMap = dbh.executeQuery(DBQuery.db4);
        System.out.println("Size of the outerMap is ::--->  " + outerMap.size());
        String Col = "Name";
        Logger().log(Status.INFO, "Query getting fired -->  " + DBQuery.db4);
        Logger().log(Status.INFO, "give the name of the column on which assert has to be applied -->  " + Col);
        List name = getDataforColumn(Col);
        expectedlist.add("Vivek");
        expectedlist.add("Mukesh");
        expectedlist.add("Gaurav");
        softassertion.assertEquals(name, expectedlist);
        softassertion.assertAll();

        // for (String x : name) {
        // System.out.println("" + name);
        // softassertion.assertEquals(name, "Amit");
        // }
    }

}
