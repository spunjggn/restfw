package com.sandeep.RESTFW.Testcases;

import org.testng.annotations.Test;

import com.sandeep.RESTFW.Setup.TestSetup;

import APIHelper.PayPalPaymentAPI;
import io.restassured.response.Response;

public class PaypalPayment extends TestSetup
{

    /*
     * This API is from Paypal and will help understand Oauth2.0 and how to retrieve token from Oauth server using
     * client id and secret and then use use the same to hit resources
     */

    public String Accesstoken;

    String code;

    Response resp;

    @Test
    public void showpaymentdetails()
    {
        resp = PayPalPaymentAPI.PayPalPaymentAuth();
        TestSetup.logDetails(resp.asString(), "response");
        System.out.println(resp.statusCode());
        System.out.println(resp.asString());
    }

}
