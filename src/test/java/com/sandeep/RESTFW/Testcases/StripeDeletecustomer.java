package com.sandeep.RESTFW.Testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sandeep.RESTFW.ResponsePOJOClasses.Root;
import com.sandeep.RESTFW.Setup.TestSetup;
import com.sandeep.RESTFW.Utils.FinalConstants;

import APIHelper.StripeCreateCustomerAPI;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class StripeDeletecustomer extends TestSetup
{

    static String custToBeDeleted;

    static Response response;

    @Test(groups = "Sanity")
    public static void deleteCustomerwithValidCustID() throws JsonParseException, JsonMappingException, IOException
    {
        /*
         * Creating a customer for deletion
         */

        custToBeDeleted = CreateCustomer.createCustomerFlowfordeletion();
        Logger().info("The cust id of the customer to be deleted is " + custToBeDeleted);

        /*
         * Deleting a valid customer
         */
        response = StripeCreateCustomerAPI.deleteCustomerAPI(FinalConstants.CREATECUSTOMER_PUBLIC_KEY, custToBeDeleted);
        JsonPath json = new JsonPath(response.asString());
        softassertion.assertEquals(json.get("id"), custToBeDeleted);
        softassertion.assertEquals(json.get("object"), "customer");
        softassertion.assertEquals(json.get("deleted"), true);
        softassertion.assertAll();

    }

    @Test
    public static void deleteCustomerWithInvalidCustID() throws JsonParseException, JsonMappingException, IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        response = StripeCreateCustomerAPI.deleteCustomerAPI(FinalConstants.CREATECUSTOMER_PUBLIC_KEY,
            FinalConstants.RETRIEVE_STRIPE_INVALID_CUST);
        System.out.println(response.asString());
        Root errorRoot = mapper.readValue(response.asString(), Root.class);
        softassertion.assertEquals(errorRoot.getError().getCode(), "resource_missing");
        softassertion.assertEquals(errorRoot.getError().getMessage(), "No such customer: 'Hoe18QzAPSxPor'");
        softassertion.assertEquals(errorRoot.getError().getParam(), "id");
        softassertion.assertAll();

    }

}
