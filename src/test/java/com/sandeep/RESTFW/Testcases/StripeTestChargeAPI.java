package com.sandeep.RESTFW.Testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.sandeep.RESTFW.Utils.FinalConstants;

import APIHelper.StripeCharge;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class StripeTestChargeAPI
{

    static Response response;

    @BeforeTest()
    public void beforeTest()
    {
        RestAssured.baseURI = "https://api.stripe.com/";
        RestAssured.basePath = "v1/";
    }

    @Test
    public static void createChargeWithValidInputs()
    {
        response =
            StripeCharge.createCharge(FinalConstants.STRIPE_CHARGES_END_POINT, FinalConstants.STRIPE_CHARGES_CURRENCY,
                FinalConstants.STRIPE_CHARGES_DESCRIPTION, FinalConstants.STRIPE_CHARGES_SOURCE,
                FinalConstants.CREATECUSTOMER_PUBLIC_KEY, FinalConstants.STRIPE_CHARGES_AMOUNT);

        System.out.println(response.asString());

    }

}
