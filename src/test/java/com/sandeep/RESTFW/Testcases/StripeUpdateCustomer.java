package com.sandeep.RESTFW.Testcases;

import java.io.IOException;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sandeep.RESTFW.RequestPOJOClasses.Customer;
import com.sandeep.RESTFW.Setup.TestSetup;
import com.sandeep.RESTFW.Utils.FinalConstants;

import APIHelper.StripeCreateCustomerAPI;
import io.restassured.response.Response;

public class StripeUpdateCustomer extends TestSetup
{
    static String custToBeUpdated;

    @Test(groups = "Sanity")
    public static void updateCustomerwithValidCustid() throws JsonParseException, JsonMappingException, IOException
    {
        custToBeUpdated = CreateCustomer.createCustomerFlowfordeletion();
        Logger().info("The cust id of the customer to be deleted is " + custToBeUpdated);

        Response response =
            StripeCreateCustomerAPI.postUpdateCustomerAPI(FinalConstants.CREATECUSTOMER_PUBLIC_KEY, custToBeUpdated);

        ObjectMapper mapper = new ObjectMapper();

        Customer customer = mapper.readValue(response.asString(), Customer.class);

        // softassertion.assertEquals(customer.getMetadata().getOrder_id(), "1234");
        softassertion.assertEquals(response.statusCode(), HttpStatus.SC_OK);

    }

}
