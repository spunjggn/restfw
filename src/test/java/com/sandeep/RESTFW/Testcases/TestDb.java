package com.sandeep.RESTFW.Testcases;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;

import com.sandeep.RESTFW.DBHelper.DataBaseHelper;
import com.sandeep.RESTFW.DBHelper.MySqlHelper;

public class TestDb
{
    public static Connection connection;

    public static void main(String[] args) throws InstantiationException, IllegalAccessException

    {

        String sqlQuery = "select * from Persons;";
        LinkedHashMap<Integer, LinkedHashMap<Object, Object>> data =
            new LinkedHashMap<Integer, LinkedHashMap<Object, Object>>();
        DataBaseHelper dbh = new MySqlHelper();

        String url = "\"jdbc:mysql://localhost:3306/employeeportal\",\"root\",\"root\"";

        try {
            dbh.setConnectionString(url);

            data = dbh.executeQuery(sqlQuery);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(data.toString());

    }

}
